<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
// use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use App\Entity\User;

class UserControllerTest extends WebTestCase {
    /**
     * @var Client
     */
    private $client;

    /**
     * @var User
     */
    private $userAdmin;

    /**
     * @var User
     */
    private $userRora;

    public function setUp() {
        //Get le User Repository pour retrouver Rora et Admin
        //Sinon on devrait créeer nos Fausses Data
        $this->client = static::createClient();
        $repoUser = $this->client->getContainer()->get('doctrine')->getManager()->getRepository(User::class);

        //Get User Admin d'id 10
        $this->userAdmin = $repoUser->findOneById(10);
        $this->assertSame(['ROLE_ADMIN'],$this->userAdmin->getRoles());

        //Get User Rora d'id 11
        $this->userRora = $repoUser->findOneById(11);
        $this->assertSame(['ROLE_USER'],$this->userRora->getRoles());
    }

    public function testApp()
    {
        // Politesse
        $this->logInUser();
        $crawler = $this->client->request('GET', '/');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Repo de Rora ne fonctionne pas pour moi
        // $repoUser = $this->client->getContainer()->get('doctrine')->getRepository(User::class);
        $repoUser = $this->client->getContainer()->get('doctrine')->getManager()->getRepository(User::class);
        $this->assertSame(11, $repoUser->count([]));
    }

    public function testAuthUserNotAdmin()
    {
        $this->logInUser();
        $crawler = $this->client->request('GET', '/');

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertSame('Here\'s our blogs', $crawler->filter('h1')->text());
        // $this->assertSame('Welcome User', $crawler->filter('h2')->text());
    }

    public function testAuthUserAdmin()
    {
        $this->logInAdmin();
        $crawler = $this->client->request('GET', '/');
        //On test le user admin
        $this->assertSame(['ROLE_ADMIN'],$this->userAdmin->getRoles());

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertSame('Here\'s our blogs', $crawler->filter('h1')->text());
        // $this->assertSame('Welcome Admin', $crawler->filter('h2')->text());
    }

    private function logInUser()
    {
        $session = $this->client->getContainer()->get('session');
        $this->assertSame(['ROLE_USER'],$this->userRora->getRoles());

        //Création du Token User Rora avec ROLE_USER
        $token = new UsernamePasswordToken($this->userRora, $this->userRora->getPassword(), 'main', $this->userRora->getRoles());
        
        //Création de la session $_SESSION qui va contenir le token
        $session->set('_security_main', serialize($token));
        $session->save();

        //Création du cookie, qui contient la session, et que l'on donne à notre client
        $this->client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));
    }

    private function logInAdmin()
    {
        $session = $this->client->getContainer()->get('session');
        $this->assertSame(['ROLE_ADMIN'],$this->userAdmin->getRoles());

        $token = new UsernamePasswordToken($this->userAdmin, $this->userAdmin->getPassword(), 'main', $this->userAdmin->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $this->client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));
    }
}