<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
// use Symfony\Component\BrowserKit\Client;
use Doctrine\ORM\Tools\SchemaTool;
use App\Tests\Controller\UserControllerTest;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\BrowserKit\Client;

use App\Entity\User;


class MyBlogControllerTest extends UserControllerTest
{
    /**
     * @var Client
     */
    public $client;


    public function testAddPostSuccess()
    {
        $this->client = static::createClient();
        // $this->client = parent::$client;
        $this->setUp();
        $this->logInUser();
        // $this->client = parent::$client;

        $crawler = $this->client->request('GET', '/my-blog/add');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $form = $crawler->selectButton('submit')->form();

        // set some values
        $form['post[title]'] = 'TITRE';
        $form['post[content]'] = 'DES TRUC';

        $crawler = $this->client->submit($form);
    }
    
    public function testAddPostFailure()
    {
        $this->client = static::createClient();
        // $this->client = parent::$client;
        $this->setUp();
        $this->logInUser();
        // $this->client = parent::$client;

        $crawler = $this->client->request('GET', '/my-blog/add');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $form = $crawler->selectButton('submit')->form();

        // set some values
        $form['post[title]'] = 'TITRE';
        $form['post[content]'] = '';

        $crawler = $this->client->submit($form);
        $this->assertSame('This value should not be blank.', $crawler->filter('span.form-error-message')->text());

    }
}