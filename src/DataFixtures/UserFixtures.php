<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker; 

//class UserFixtures extends AbstractFixture implements ContainerAwareInterface, 
//FixtureInterface, OrderedFixtureInterface //Fixture ou BaseFixture
class UserFixtures extends Fixture
{
    // for creation passwords
    private $encoder;
    private $faker;

    public const USER_REFERENCE = 'user';
    
    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
        $this->faker = Faker\Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager)
    {
        // create 5 users classiques
        // for ($i = 1; $i < 6; $i++) {
        //     $user = new User();
        //     $user->setUsername('User ' . $i);
        //     $user->setPassword($this->encoder->encodePassword($user, '12345'));
        //     $user->setRole('ROLE_USER');
        //     $manager->persist($user);
        //     //On ajoute chaque user en référence pour d'autres fixtures
        //     $this->addReference(self::USER_REFERENCE.$i, $user);
        // }

        //On créer 5 users classiques avec faker
        for ($x = 1; $x < 6; $x++) {
            $user = new User();
            $user->setUsername($this->faker->userName); //name ou firstName ou LastName ou userName
            // $this->faker->mail
            $user->setPassword($this->encoder->encodePassword($user, '12345'));
            // ou $this->faker->password
            $user->setRole('ROLE_USER');
            $manager->persist($user);
            //On ajoute chaque user en référence pour d'autres fixtures
            $this->addReference(self::USER_REFERENCE.$x, $user);
        }       

        // create admin
        $user = new User();
        $user->setUsername('Romy');
        $user->setPassword($this->encoder->encodePassword($user, 'romy1'));
        $user->setRole('ROLE_ADMIN');
        $manager->persist($user);

        // create user role user for our test
        $user11 = new User();
        $user11->setUsername('Test');
        $user11->setPassword($this->encoder->encodePassword($user11, '12345'));
        $user11->setRole('ROLE_USER');
        $manager->persist($user11);
    

        $manager->flush(); // start of all this class AppFixtures code
    }

    public function getOrder()
    {
        return 1;
    }

}